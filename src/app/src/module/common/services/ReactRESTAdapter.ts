import {Observable} from "rxjs/Observable";
import {injectable} from "inversify";

import {RESTAdapterInterface, RESTAdapterInterfaceOptions} from "../../../definitions/essentials/RESTAdapterInterface";

@injectable()
export class ReactRESTAdapter implements RESTAdapterInterface {
    get (url: string, options?: RESTAdapterInterfaceOptions): Observable<any> {
        throw new Error('Not implemented');
    }

    put(url: string, json?: any, options?: RESTAdapterInterfaceOptions): Observable<any> {
        throw new Error('Not implemented');
    }

    post(url: string, json?: any, options?: RESTAdapterInterfaceOptions): Observable<any> {
        throw new Error('Not implemented');
    }

    delete(url: string, options?: RESTAdapterInterfaceOptions): Observable<any> {
        throw new Error('Not implemented');
    }
}