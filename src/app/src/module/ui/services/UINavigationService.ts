import {injectable} from "inversify";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

export interface UINavigationPage
{
    displayInNavbar: boolean,
    title: {
        formattedMessage: {
            id: string,
            defaultMessage: string,
        },
    },
}

@injectable()
export class UINavigationService
{
    public onChanges: BehaviorSubject<UINavigationPage[]> = new BehaviorSubject<UINavigationPage[]>([]);

    private pages: UINavigationPage[] = [];

    push(page: UINavigationPage): void {
        this.pages.push(this.makePageClone(page));
        this.emitOnChanges();
    }

    pop(): void {
        this.pages.pop();
        this.emitOnChanges();
    }

    all(): UINavigationPage[] {
        return this.pages.map(p => this.makePageClone(p));
    }

    private emitOnChanges(): void {
        this.onChanges.next(this.all());
    }

    private makePageClone(input: UINavigationPage): UINavigationPage {
        return {
            displayInNavbar: input.displayInNavbar,
            title: {
                formattedMessage: {
                    id: input.title.formattedMessage.id,
                    defaultMessage: input.title.formattedMessage.defaultMessage,
                },
            }
        }
    }
}