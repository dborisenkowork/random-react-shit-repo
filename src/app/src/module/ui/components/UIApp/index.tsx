import * as React from "react";

import './style.less';

import {BrowserRouter as Router, Route} from 'react-router-dom';
import {IntlProvider} from 'react-intl';

import {LDT} from "../../../../module";

export class UIApp extends React.Component<undefined, undefined> {
    render(): JSX.Element {
        return (
            <IntlProvider locale="en">
                <Router>
                    <div className="ldt___ui-app">
                        <div className="ldt___ui-app__title">
                            <LDT.Modules.UI.Components.UITitle/>
                        </div>
                        <div className="ldt___ui-app__window">
                            <div className="ldt___ui-app__window__main-menu">
                                <LDT.Modules.UI.Components.UIMainMenu/>
                            </div>
                            <div className="ldt___ui-app__window__contents">
                                <Route exact path="/" component={LDT.Routes.Public.Index}/>
                                <Route path="/public" component={LDT.Routes.Public.Index}/>
                                <Route path="/protected" component={LDT.Routes.Protected.Index}/>
                            </div>
                        </div>
                    </div>
                </Router>
            </IntlProvider>
        );
    }
}