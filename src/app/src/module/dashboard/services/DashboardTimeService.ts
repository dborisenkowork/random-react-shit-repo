import {BehaviorSubject} from "rxjs/BehaviorSubject";

import {injectable} from "inversify";
import {Observable} from 'rxjs/Rx';

export interface NextTimeResponse
{
    hours: number,
    minutes: number,
    seconds: number,
}

@injectable()
export class DashboardTimeService
{
    public current: BehaviorSubject<NextTimeResponse> = new BehaviorSubject<NextTimeResponse>(this.getNext());

    constructor() {
        Observable.interval(1000).subscribe(() => {
            this.current.next(this.getNext());
        });
    }

    getNext(): NextTimeResponse {
        let current = new Date();

        return {
            hours: current.getHours(),
            minutes: current.getMinutes(),
            seconds: current.getSeconds(),
        };
    }
}