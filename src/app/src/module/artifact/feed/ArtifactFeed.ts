import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";

import {lazyInject} from "../../../config-di/di-common";
import {ArtifactRepository, ArtifactRepositoryEntity} from "../repository/ArtifactRepository";
import {injectable} from "inversify";

export interface ArtifactFeedQuery { undefined }

export interface ArtifactFeedItem {
    id: string,
    entity$: BehaviorSubject<ArtifactRepositoryEntity>,
}

@injectable()
export class ArtifactFeed
{
    @lazyInject(ArtifactRepository) repository: ArtifactRepository;

    private unsubscribe: Subject<void> = new Subject<void>();

    private _items: ArtifactFeedItem[] = [];
    private _view: Subject<ArtifactFeedItem[]> = new Subject<ArtifactFeedItem[]>();

    constructor(
        private initialQuery: ArtifactFeedQuery,
    ) {
        let nextAll: Subject<void> = new Subject<void>();

        this.repository.store.all$
            .takeUntil(this.unsubscribe)
            .subscribe(
                (next) => {
                    nextAll.next(undefined);

                    let result: ArtifactFeedItem[] = [];

                    for(let i in next) {
                        if(next.hasOwnProperty(i)) {
                            result.push({
                                id: i,
                                entity$: next[i],
                            });
                        }
                    }

                    this._items = result;
                    this.update();
                },
            );
    }

    get view(): Subject<ArtifactFeedItem[]> {
        return this._view;
    }

    takeUntil(until: Observable<any>): void {
        until.subscribe(next => this.unsubscribe.next(undefined));
    }

    fetchAll(): Observable<void> {
        return this.repository.fetchAll();
    }

    update(): void {
        this._view.next(this._items);
    }
}

export function factoryArtifactFeed(initialQuery: ArtifactFeedQuery): ArtifactFeed {
    return new ArtifactFeed(initialQuery);
}