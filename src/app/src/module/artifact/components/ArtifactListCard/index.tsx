import * as React from "react";

import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Subject} from "rxjs/Subject";

import {Artifact} from "../../../../definitions/ldt-api/artifacts/entity/ArtifactEntity";

interface ArtifactListCardProps
{
    artifact$:  BehaviorSubject<Artifact>,
}

interface ArtifactListCardState
{
    artifact: Artifact,
}

export class ArtifactListCard extends React.Component<ArtifactListCardProps, ArtifactListCardState>
{
    private unmount: Subject<void> = new Subject<void>();

    componentWillMount(): void {
        this.props.artifact$
            .takeUntil(this.unmount)
            .subscribe(next => {
                this.setState({
                    artifact: next,
                })
            });
    }

    componentWillUnmount(): void {
        this.unmount.next(undefined);
    }

    render(): JSX.Element {
        if(this.state && this.state.artifact) {
            return (
                <div>
                    <h1>{this.state.artifact.entity.code}</h1>
                    <h2>{this.state.artifact.entity.title}</h2>
                    <p>{this.state.artifact.entity.description}</p>
                </div>
            );
        }else{
            return (
                <div>No artifact available</div>
            );
        }
    }
}