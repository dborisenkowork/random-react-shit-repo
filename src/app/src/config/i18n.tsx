import {addLocaleData} from 'react-intl';

const ru = require('react-intl/locale-data/ru');

addLocaleData(ru);