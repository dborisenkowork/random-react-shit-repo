import * as React from "react";

import {LDT} from "../../../../module";
import {lazyInject} from "../../../../config-di/di-common";
import {UINavigationService} from "../../../../module/ui/services/UINavigationService";

export class ProtectedArtifactsRoute extends React.Component<undefined, undefined>
{
    @lazyInject(UINavigationService) private uiNavigation: UINavigationService;

    componentWillMount(): void {
        this.uiNavigation.push({
            displayInNavbar: true,
            title: {
                formattedMessage: {
                    id: 'routes.protected.artifacts.title',
                    defaultMessage: 'Artifacts'
                }
            }
        });
    }

    componentWillUnmount(): void {
        this.uiNavigation.pop();
    }

    render(): JSX.Element {
        return <div>
            <h1>Artifacts</h1>
            <div>
                <LDT.Modules.Artifact.Components.ArtifactsBrowser/>
            </div>
        </div>;
    }
}