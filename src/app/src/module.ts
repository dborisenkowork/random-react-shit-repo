import {ProtectedRoute} from "./routes/ProtectedRoute/index";
import {ProtectedArtifactsRoute} from "./routes/ProtectedRoute/routes/ArtifactsRoute/index";
import {PublicRoute} from "./routes/PublicRoute/index";
import {ProtectedDashboardRoute} from "./routes/ProtectedRoute/routes/DashboardRoute/index";
import {PublicAuthSignOutRoute} from "./routes/PublicRoute/routes/AuthRoute/routes/SignOutRoute/index";
import {UIApp} from "./module/ui/components/UIApp/index";
import {UIMainMenu} from "./module/ui/components/UIMainMenu/index";
import {UITitle} from "./module/ui/components/UITitle/index";
import {DashboardClock} from "./module/dashboard/components/DashboardClock/index";
import {ArtifactList} from "./module/artifact/components/ArtifactList/index";
import {ArtifactListItem} from "./module/artifact/components/ArtifactListItem/index";
import {PublicAuthRoute} from "./routes/PublicRoute/routes/AuthRoute/index";
import {ArtifactListCard} from "./module/artifact/components/ArtifactListCard/index";
import {ArtifactsBrowser} from "./module/artifact/components/ArtifactsBrowser/index";

export const LDT = {
    Routes: {
        Protected: {
            Index: ProtectedRoute,
            Artifacts: {
                Index: ProtectedArtifactsRoute,
            },
            Dashboard: {
                Index: ProtectedDashboardRoute,
            }
        },
        Public: {
            Index: PublicRoute,
            Auth: {
                Index: PublicAuthRoute,
                SignOut: PublicAuthSignOutRoute,
            }
        }
    },
    Modules: {
        Artifact: {
            Components: {
                ArtifactList: ArtifactList,
                ArtifactListItem: ArtifactListItem,
                ArtifactListCard: ArtifactListCard,
                ArtifactsBrowser: ArtifactsBrowser,
            }
        },
        UI: {
            Components: {
                UIApp: UIApp,
                UIMainMenu: UIMainMenu,
                UITitle: UITitle,
            }
        },
        Dashboard: {
            Components: {
                DashboardClock: DashboardClock,
            },
        }
    }
};