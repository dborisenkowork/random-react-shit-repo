import {Observable} from "rxjs/Observable";

import {ArtifactsGetAllResponse200, urlArtifactsGetAll} from "../path/get-all";
import {RESTAdapterInterface} from "../../../essentials/RESTAdapterInterface";

export interface ArtifactRESTServiceInterface
{
    getAll(): Observable<ArtifactsGetAllResponse200>;
}

export class ArtifactRESTService implements ArtifactRESTServiceInterface
{
    constructor(
        private adapter: RESTAdapterInterface,
    ) {}

    getAll(): Observable<ArtifactsGetAllResponse200> {
        return this.adapter.get(urlArtifactsGetAll);
    }
}